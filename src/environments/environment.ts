// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebaseConfig : {
    apiKey: "AIzaSyCuPSNoRJxaFUBpTYRol9S-Gb-d9yZQHv4",
    authDomain: "crud-employee-aea91.firebaseapp.com",
    projectId: "crud-employee-aea91",
    storageBucket: "crud-employee-aea91.appspot.com",
    messagingSenderId: "338872360298",
    appId: "1:338872360298:web:0243059348d816dc11d913"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
