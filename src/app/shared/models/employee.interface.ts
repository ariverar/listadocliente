export interface Employee {
    nombre: string,
    apellido: string,
    edad: number,
    fecha: string
}
