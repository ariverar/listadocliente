import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Employee } from '../../models/employee.interface';

@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.scss']
})
export class EmployeeFormComponent implements OnInit {

  employee: Employee;
  employeeForm: FormGroup;


  constructor(private router: Router, private fb: FormBuilder) {
    const navigation = this.router.getCurrentNavigation();
    this.employee = navigation?.extras?.state?.value;
    


  }

  ngOnInit(): void {
    this.initForm();
    if (typeof this.employee === 'undefined') {
      this.router.navigate(['new'])

    } else {

    }
  }

  onSave(): void {
    console.log('salvado');
  }

   initForm(): void {
    this.employeeForm = this.fb.group(
      {
        nombre: ['', [Validators.required]],
        apellido: ['', [Validators.required]],
        edad: ['', [Validators.required]],
        fecha: ['', [Validators.required]]
      }
    )
  }


}
