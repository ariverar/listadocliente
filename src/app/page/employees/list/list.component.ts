import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  navigationExtras = {
    state: {
      value: null
    }
  };

  fakeData = [
    {
      nombre: "llLALALAL",
      apellido: "apellidodddd",
      edad: 36,
      fecha: "12/12/2018"
    },
    {
      nombre: "llLALA22LAL",
      apellido: "apell222idodddd",
      edad: 36,
      fecha: "12/12/2018"
    },
    {
      nombre: "ll333LALALAL",
      apellido: "apel333lidodddd",
      edad: 36,
      fecha: "12/12/2018"
    }
  ];

  constructor(private route: Router) { }

  ngOnInit(): void {
  }

  onGoToSee(item: any): void {
    this.navigationExtras.state.value = item;
    this.route.navigate(['details'], this.navigationExtras);
    console.log(item)

  }

}
