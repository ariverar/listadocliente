import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Employee } from '../../shared/models/employee.interface';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } form 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EmployeesService {
  employee: Observable<Employee[]>;
  private employeeCollection: AngularFirestoreCollection<Employee>;

  constructor(private readonly afs: AngularFirestore) {

    this.employeeCollection = afs.collection<Employee>('employees');
    this.getEmployees();


  }


  onSaveEmploye(employee: Employee, empId: string): Promise<void> {

    return new Promise(async (resolcer, reject) => {


      try {
        const id = empId || this.afs.createId();
        const data = { id, ...employee };
        const result = await this.employeeCollection.doc(id).set(data);
        resolve(result);

      } catch (error) {
        reject(error.message);

      }
    });


  }
  private getEmployees(): void {

    this.getEmployees = this.employeeCollection.snapshotChanges().pipe(

      map(actions => actions.map(a => a.payload.doc.data() as Employee))
    )

  }

}
